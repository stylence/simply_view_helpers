<?php
namespace Stylence\SimplyViewHelpers\ViewHelpers;

class CheckboxStatusViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
	/**
	 * @param array $selectedCategories
	 * @param integer $current
	 * @return string
	 */
	public function render($selectedCategories, $current) {
		foreach ($selectedCategories as $category) {
			if($category->getUid() === $current) return 'checked';
		}
		return '';
	}
}