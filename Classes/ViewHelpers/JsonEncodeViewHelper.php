<?php
namespace Stylence\SimplyViewHelpers\ViewHelpers;

class JsonEncodeViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param null $objects
	 * @param string $label
	 * @return string json encoded string
	 */
	public function render($label, $objects = NULL) {
		if ($objects === NULL) {
			$objects = $this->renderChildren();
		}
		$objectsArray = array();
		$getter = 'get' . ucfirst($label);
		foreach ($objects as $object) {
			if($object->$getter()) {
				$objectsArray[] = $object->$getter();
			}
		}
		return json_encode(array_merge(array(), array_unique($objectsArray)));
	}
}