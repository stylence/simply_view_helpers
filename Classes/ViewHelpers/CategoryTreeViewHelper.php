<?php
namespace Stylence\SimplyViewHelpers\ViewHelpers;
	/* * *************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2011 Henjo Hoeksma <me@henjohoeksma.nl>, Stylence
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 2 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 * ************************************************************* */

	/**
	 * Project
	 *
	 * @version $Id$
	 * @copyright Copyright belongs to the respective authors
	 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
	 */

	class CategoryTreeViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

		/**
		 *
		 * @param integer $category
		 * @param string $as The name of the iteration variable
		 * @param integer $key
		 * @return string
		 */
		public function render($category, $as, $key) {
			$output = '';
			$categoryRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Domain\Repository\CategoryRepository'); // Singleton
			$categories = $categoryRepository->findByParent($category);
			foreach ($categories as $keyValue => $value) {
				$this->templateVariableContainer->add($as, $value);
				$this->templateVariableContainer->add($key, $keyValue);
				$output .= $this->renderChildren();
				$this->templateVariableContainer->remove($as);
				$this->templateVariableContainer->remove($key);
			}
			return $output;
		}

	}

?>